/**
 * Catalogue model events
 */

'use strict';

import {EventEmitter} from 'events';
var CatalogueEvents = new EventEmitter();

// Set max event listeners (0 == unlimited)
CatalogueEvents.setMaxListeners(0);

// Model events
var events = {
  save: 'save',
  remove: 'remove'
};

// Register the event emitter to the model events
function registerEvents(Catalogue) {
  for(var e in events) {
    let event = events[e];
    Catalogue.post(e, emitEvent(event));
  }
}

function emitEvent(event) {
  return function(doc) {
    CatalogueEvents.emit(event + ':' + doc._id, doc);
    CatalogueEvents.emit(event, doc);
  };
}

export {registerEvents};
export default CatalogueEvents;
