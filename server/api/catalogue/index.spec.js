'use strict';

/* globals sinon, describe, expect, it */

var proxyquire = require('proxyquire').noPreserveCache();

var catalogueCtrlStub = {
  index: 'catalogueCtrl.index',
  show: 'catalogueCtrl.show',
  create: 'catalogueCtrl.create',
  upsert: 'catalogueCtrl.upsert',
  patch: 'catalogueCtrl.patch',
  destroy: 'catalogueCtrl.destroy'
};

var routerStub = {
  get: sinon.spy(),
  put: sinon.spy(),
  patch: sinon.spy(),
  post: sinon.spy(),
  delete: sinon.spy()
};

// require the index with our stubbed out modules
var catalogueIndex = proxyquire('./index.js', {
  express: {
    Router() {
      return routerStub;
    }
  },
  './catalogue.controller': catalogueCtrlStub
});

describe('Catalogue API Router:', function() {
  it('should return an express router instance', function() {
    catalogueIndex.should.equal(routerStub);
  });

  describe('GET /api/catalogues', function() {
    it('should route to catalogue.controller.index', function() {
      routerStub.get
        .withArgs('/', 'catalogueCtrl.index')
        .should.have.been.calledOnce;
    });
  });

  describe('GET /api/catalogues/:id', function() {
    it('should route to catalogue.controller.show', function() {
      routerStub.get
        .withArgs('/:id', 'catalogueCtrl.show')
        .should.have.been.calledOnce;
    });
  });

  describe('POST /api/catalogues', function() {
    it('should route to catalogue.controller.create', function() {
      routerStub.post
        .withArgs('/', 'catalogueCtrl.create')
        .should.have.been.calledOnce;
    });
  });

  describe('PUT /api/catalogues/:id', function() {
    it('should route to catalogue.controller.upsert', function() {
      routerStub.put
        .withArgs('/:id', 'catalogueCtrl.upsert')
        .should.have.been.calledOnce;
    });
  });

  describe('PATCH /api/catalogues/:id', function() {
    it('should route to catalogue.controller.patch', function() {
      routerStub.patch
        .withArgs('/:id', 'catalogueCtrl.patch')
        .should.have.been.calledOnce;
    });
  });

  describe('DELETE /api/catalogues/:id', function() {
    it('should route to catalogue.controller.destroy', function() {
      routerStub.delete
        .withArgs('/:id', 'catalogueCtrl.destroy')
        .should.have.been.calledOnce;
    });
  });
});
