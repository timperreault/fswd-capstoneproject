'use strict';

import mongoose from 'mongoose';
import {registerEvents} from './catalogue.events';

var CatalogueSchema = new mongoose.Schema({
    make: {
        type: String,
        required: true
    },
    model:{
        type: String,
        required: true
    },
    caliber: String,
    name:String,
    category: {
        type: String,
        required: true
    },
    active: {
        type: Boolean,
        default: true
    }
});

registerEvents(CatalogueSchema);
export default mongoose.model('Catalogue', CatalogueSchema);
