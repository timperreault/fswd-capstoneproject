'use strict';

/* globals describe, expect, it, beforeEach, afterEach */
//
var app = require('../..');
import request from 'supertest';

var newCatalogue;

describe('Catalogue API:', function() {
  describe('GET /api/catalogues', function() {
    var catalogues;

    beforeEach(function(done) {
      request(app)
        .get('/api/catalogues')
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          catalogues = res.body;
          done();
        });
    });

    it('should respond with JSON array', function() {
      catalogues.should.be.instanceOf(Array);
    });
  });

  describe('POST /api/catalogues', function() {
    beforeEach(function(done) {
      request(app)
        .post('/api/catalogues')
        .send({
          name: 'New Catalogue',
          info: 'This is the brand new catalogue!!!'
        })
        .expect(201)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          newCatalogue = res.body;
          done();
        });
    });

    it('should respond with the newly created catalogue', function() {
      newCatalogue.name.should.equal('New Catalogue');
      newCatalogue.info.should.equal('This is the brand new catalogue!!!');
    });
  });

  describe('GET /api/catalogues/:id', function() {
    var catalogue;

    beforeEach(function(done) {
      request(app)
        .get(`/api/catalogues/${newCatalogue._id}`)
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          catalogue = res.body;
          done();
        });
    });

    afterEach(function() {
      catalogue = {};
    });

    it('should respond with the requested catalogue', function() {
      catalogue.name.should.equal('New Catalogue');
      catalogue.info.should.equal('This is the brand new catalogue!!!');
    });
  });

  describe('PUT /api/catalogues/:id', function() {
    var updatedCatalogue;

    beforeEach(function(done) {
      request(app)
        .put(`/api/catalogues/${newCatalogue._id}`)
        .send({
          name: 'Updated Catalogue',
          info: 'This is the updated catalogue!!!'
        })
        .expect(200)
        .expect('Content-Type', /json/)
        .end(function(err, res) {
          if(err) {
            return done(err);
          }
          updatedCatalogue = res.body;
          done();
        });
    });

    afterEach(function() {
      updatedCatalogue = {};
    });

    it('should respond with the updated catalogue', function() {
      updatedCatalogue.name.should.equal('Updated Catalogue');
      updatedCatalogue.info.should.equal('This is the updated catalogue!!!');
    });

    it('should respond with the updated catalogue on a subsequent GET', function(done) {
      request(app)
        .get(`/api/catalogues/${newCatalogue._id}`)
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          let catalogue = res.body;

          catalogue.name.should.equal('Updated Catalogue');
          catalogue.info.should.equal('This is the updated catalogue!!!');

          done();
        });
    });
  });

  describe('PATCH /api/catalogues/:id', function() {
    var patchedCatalogue;

    beforeEach(function(done) {
      request(app)
        .patch(`/api/catalogues/${newCatalogue._id}`)
        .send([
          { op: 'replace', path: '/name', value: 'Patched Catalogue' },
          { op: 'replace', path: '/info', value: 'This is the patched catalogue!!!' }
        ])
        .expect(200)
        .expect('Content-Type', /json/)
        .end(function(err, res) {
          if(err) {
            return done(err);
          }
          patchedCatalogue = res.body;
          done();
        });
    });

    afterEach(function() {
      patchedCatalogue = {};
    });

    it('should respond with the patched catalogue', function() {
      patchedCatalogue.name.should.equal('Patched Catalogue');
      patchedCatalogue.info.should.equal('This is the patched catalogue!!!');
    });
  });

  describe('DELETE /api/catalogues/:id', function() {
    it('should respond with 204 on successful removal', function(done) {
      request(app)
        .delete(`/api/catalogues/${newCatalogue._id}`)
        .expect(204)
        .end(err => {
          if(err) {
            return done(err);
          }
          done();
        });
    });

    it('should respond with 404 when catalogue does not exist', function(done) {
      request(app)
        .delete(`/api/catalogues/${newCatalogue._id}`)
        .expect(404)
        .end(err => {
          if(err) {
            return done(err);
          }
          done();
        });
    });
  });
});
