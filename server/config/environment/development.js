'use strict';
/*eslint no-process-env:0*/

// Development specific configuration
// ==================================
module.exports = {

  // MongoDB connection options
  mongo: {
    uri: 'mongodb://appConnector:8MnknBhoalyCJXz4@cluster0-shard-00-00-kgwub.mongodb.net:27017,cluster0-shard-00-01-kgwub.mongodb.net:27017,cluster0-shard-00-02-kgwub.mongodb.net:27017/test?ssl=true&replicaSet=Cluster0-shard-0&authSource=admin'
  },

  // Seed database on startup
  seedDB: true

};
