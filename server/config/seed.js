/**
 * Populate DB with sample data on server start
 * to disable, edit config/environment/index.js, and set `seedDB: false`
 */

'use strict';
import Catalogue from '../api/catalogue/catalogue.model';
import Thing from '../api/thing/thing.model';
import User from '../api/user/user.model';
import config from './environment/';

export default function seedDatabaseIfNeeded() {
    if(config.seedDB) {
        Thing.find({}).remove()
            .then(() => {
            let thing = Thing.create({
                name: 'Development Tools',
                info: 'Integration with popular tools such as Webpack, Gulp, Babel, TypeScript, Karma, '
                + 'Mocha, ESLint, Node Inspector, Livereload, Protractor, Pug, '
                + 'Stylus, Sass, and Less.'
            }, {
                name: 'Server and Client integration',
                info: 'Built with a powerful and fun stack: MongoDB, Express, '
                + 'AngularJS, and Node.'
            }, {
                name: 'Smart Build System',
                info: 'Build system ignores `spec` files, allowing you to keep '
                + 'tests alongside code. Automatic injection of scripts and '
                + 'styles into your index.html'
            }, {
                name: 'Modular Structure',
                info: 'Best practice client and server structures allow for more '
                + 'code reusability and maximum scalability'
            }, {
                name: 'Optimized Build',
                info: 'Build process packs up your templates as a single JavaScript '
                + 'payload, minifies your scripts/css/images, and rewrites asset '
                + 'names for caching.'
            }, {
                name: 'Deployment Ready',
                info: 'Easily deploy your app to Heroku or Openshift with the heroku '
                + 'and openshift subgenerators'
            });
            return thing;
        })
            .then(() => console.log('finished populating things'))
            .catch(err => console.log('error populating things', err));

        User.find({}).remove()
            .then(() => {
            User.create({
                provider: 'local',
                name: 'Test User',
                email: 'test@example.com',
                password: 'test'
            }, {
                provider: 'local',
                role: 'admin',
                name: 'Admin',
                email: 'admin@example.com',
                password: 'admin'
            })
                .then(() => console.log('finished populating users'))
                .catch(err => console.log('error populating users', err));
        });
        Catalogue.find({}).remove()
            .then(() => {
            let catalogue = Catalogue.create({
                make: 'Mossberg',
                model: 'MVP LC',
                caliber: '5.56 NATO',
                name: 'Mossberg MVP LC',
                category: 'firearm',
                active: true
            }, {
                make: 'Sako',
                model: 'TRG 42',
                caliber: '.338 Lapua Magnum',
                name: 'Sako TRG 42',
                category: 'firearm',
                active: true
            },{
                make: 'Federal ',
                model: 'American Eagle',
                caliber: '5.56 NATO',
                name: 'Federal American Eagle 5.56 NATO',
                category: 'ammunition',
                active: true
            },{
                make: 'Schmidt & Bender',
                model: 'PM II',
                name: 'Schmidt & Bender PM II 3.0-20x',
                category: 'optic',
                active: true
            },{
                make: 'M&P',
                model: '15 Sport II',
                caliber: '5.56NATO',
                name: 'M&P 15 Sport II',
                category: 'firearm',
                active: true
            },{
                make: 'M&P',
                model: '15 Sport',
                caliber: '5.56NATO',
                name: 'M&P 15 Sport',
                category: 'firearm',
                active: true
            },{
                make: 'Remmington',
                model: '700 5-R Mil-Spec',
                caliber: '.308 Winchester',
                name: 'Remmington 700 5-R',
                category: 'firearm',
                active: true
            },{
                make: 'Remmington',
                model: '700P MLR',
                caliber: '.338 Lapua Magnum',
                name: 'Remmington 700P MLR',
                category: 'firearm',
                active: true
            },{
                make: 'Remmington',
                model: '700 XCR Tactical Long Range',
                caliber: '.308 Winchester',
                name: 'Remmington 700 XCR',
                category: 'firearm',
                active: true
            });
            return catalogue;
            })
        .then(() => console.log('finished populating catalogue'))
        .catch(err => console.log('error populating catalogue', err));
    }
}
