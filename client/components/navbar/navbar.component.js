'use strict';
/* eslint no-sync: 0 */

import angular from 'angular';

export class NavbarComponent {
    menu = [
        {
            title: 'Dashboard',
            state: 'dashboard',
            icon: 'glyphicon glyphicon-home'
        },
        {
            title: 'Catalogue',
            state: 'catalogue',
            icon: 'glyphicon glyphicon-list-alt'
        },
        {
            title: 'Logbook',
            state: 'logbook',
            icon: 'glyphicon glyphicon-screenshot'
        },
        {
            title: 'Analytics',
            state: 'analytics',
            icon: 'glyphicon glyphicon-stats'
        }];

isCollapsed = true;

constructor(Auth) {
    'ngInject';

    this.isLoggedIn = Auth.isLoggedInSync;
    this.isAdmin = Auth.isAdminSync;
    this.getCurrentUser = Auth.getCurrentUserSync;
}

}

export default angular.module('directives.navbar', [])
    .component('navbar', {
    template: require('./navbar.html'),
    controller: NavbarComponent
})
    .name;
