'use strict';
const angular = require('angular');

const uiRouter = require('angular-ui-router');

import routes from './analytics.routes';

export class AnalyticsComponent {
  /*@ngInject*/
  constructor() {
    this.message = 'Hello';
  }
}

export default angular.module('fullstackLogbookApp.analytics', [uiRouter])
  .config(routes)
  .component('analytics', {
    template: require('./analytics.html'),
    controller: AnalyticsComponent,
    controllerAs: 'analyticsCtrl'
  })
  .name;
