'use strict';

describe('Component: AnalyticsComponent', function() {
  // load the controller's module
  beforeEach(module('fullstackLogbookApp.analytics'));

  var AnalyticsComponent;

  // Initialize the controller and a mock scope
  beforeEach(inject(function($componentController) {
    AnalyticsComponent = $componentController('analytics', {});
  }));

  it('should ...', function() {
    expect(1).toEqual(1);
  });
});
