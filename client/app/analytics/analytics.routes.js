'use strict';

export default function($stateProvider) {
  'ngInject';
  $stateProvider
    .state('analytics', {
      url: '/analytics',
      template: '<analytics></analytics>'
    });
}
