'use strict';
const angular = require('angular');

const uiRouter = require('angular-ui-router');

import routes from './catalogue.routes';

export class CatalogueComponent {

    userCatalogue = [];
newCatalogueItem = '';
editingMode = false;
addMode = false;

/*@ngInject*/
constructor($http) {
    this.$http = $http;
}

$onInit() {
    this.$http.get('/api/catalogues')
        .then(response => {
        var theResponse = response.data;
        for (var i = 0; i < theResponse.length; i++) {
            if (theResponse[i].active)
                this.userCatalogue.push(theResponse[i]);
        }
    });
}

toggleEdit(){
    if(this.editingMode)
        this.editingMode = false;
    else
        this.editingMode = true;
}

toggleAdd(){
    if(this.addMode)
        this.addMode = false;
    else
        this.addMode = true;
}

saveNewFirearm() {
    var newFirearm = {};
    newFirearm.make = inputMake.value;
    newFirearm.model = inputModel.value;
    newFirearm.caliber = inputCaliber.value;
    newFirearm.name = inputName.value;
    newFirearm.category = category;
    this.$http.post(`/api/catalogues`, newFirearm)
        .then(resp => {
        console.log(resp.data);
        console.log(this.userCatalogue);
        this.userCatalogue.push(resp.data);   
    })
        .catch(err => {
        console.error(err);
    });
    this.toggleAdd();
}

saveNewAmmunition() {
    var newAmmunition = {};
    newAmmunition.make = inputAmmoMake.value;
    newAmmunition.model = inputAmmoModel.value;
    newAmmunition.caliber = inputAmmoCaliber.value;
    newAmmunition.name = inputAmmoName.value;
    newAmmunition.category = 'ammunition';
    this.$http.post(`/api/catalogues`, newAmmunition)
        .then(resp => {
        console.log(resp.data);
        console.log(this.userCatalogue);
        this.userCatalogue.push(resp.data);   
    })
        .catch(err => {
        console.error(err);
    });
    this.toggleAdd();
}

saveNewOptic() {
    var newOptic = {};
    newOptic.make = inputOpticMake.value;
    newOptic.model = inputOpticModel.value;
    newOptic.name = inputOpticName.value;
    newOptic.category = 'optic';
    this.$http.post(`/api/catalogues`, newOptic)
        .then(resp => {
        console.log(resp.data);
        console.log(this.userCatalogue);
        this.userCatalogue.push(resp.data);   
    })
        .catch(err => {
        console.error(err);
    });
    this.toggleAdd();
}


deactivateItem(item){
    if(item){
        if(confirm(`Are you sure you want to remove ${item.name} from your catalogue?`)){
            item.active = false;
            this.$http.put(`/api/catalogues/${item._id}`, item)
                .then(function(data){
                return data;
            }, function(err){});
            var index = this.userCatalogue.indexOf(item);
            console.log("Index: " + index);
            if (index != -1 ) {
                this.userCatalogue.splice(index, 1);
            }
            this.toggleEdit();
        }
    }
}
}

export default angular.module('fullstackLogbookApp.catalogue', [uiRouter])
    .config(routes)
    .component('catalogue', {
    template: require('./catalogue.html'),
    controller: CatalogueComponent,
    controllerAs: 'catalogueCtrl'
})
    .name;
