'use strict';

export default function($stateProvider) {
  'ngInject';
  $stateProvider
    .state('catalogue', {
      url: '/catalogue',
      template: '<catalogue></catalogue>'
    });
}
