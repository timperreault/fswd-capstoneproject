'use strict';

describe('Component: CatalogueComponent', function() {
  // load the controller's module
  beforeEach(module('fullstackLogbookApp.catalogue'));

  var CatalogueComponent;

  // Initialize the controller and a mock scope
  beforeEach(inject(function($componentController) {
    CatalogueComponent = $componentController('catalogue', {});
  }));

  it('should ...', function() {
    expect(1).toEqual(1);
  });
});
