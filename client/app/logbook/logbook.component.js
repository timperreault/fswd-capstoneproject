'use strict';
const angular = require('angular');

const uiRouter = require('angular-ui-router');

import routes from './logbook.routes';

export class LogbookComponent {
  /*@ngInject*/
  constructor() {
    this.message = 'Hello';
  }
}

export default angular.module('fullstackLogbookApp.logbook', [uiRouter])
  .config(routes)
  .component('logbook', {
    template: require('./logbook.html'),
    controller: LogbookComponent,
    controllerAs: 'logbookCtrl'
  })
  .name;
