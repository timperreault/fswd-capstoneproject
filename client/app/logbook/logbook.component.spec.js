'use strict';

describe('Component: LogbookComponent', function() {
  // load the controller's module
  beforeEach(module('fullstackLogbookApp.logbook'));

  var LogbookComponent;

  // Initialize the controller and a mock scope
  beforeEach(inject(function($componentController) {
    LogbookComponent = $componentController('logbook', {});
  }));

  it('should ...', function() {
    expect(1).toEqual(1);
  });
});
