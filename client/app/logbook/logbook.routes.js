'use strict';

export default function($stateProvider) {
  'ngInject';
  $stateProvider
    .state('logbook', {
      url: '/logbook',
      template: '<logbook></logbook>'
    });
}
